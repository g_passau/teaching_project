#!/usr/bin/env python
# -*- coding: utf-8 -*-
# import all the functions needed
from sys import argv
import codecs
import functions.tokens as tokens
import functions.sentences as sentences
import functions.annotation as annotation
import functions.lemma as lemma

import sys
reload(sys)
sys.setdefaultencoding("utf-8")
# print instructions
print "   "
print "*******************************"
print "   "
print """Hello - this is a very useful programme that can do a bunch of really great things. Believe it or not!
It can tokenise a given text, annotate it and then finally even calculate the ration between two different given types of words! Isn't that great?
Give it a try and enjoy this master piece of programming art.

So first decide whether you want to load a text file (.txt) to work on (1) or copy paste your text here in this console (2).
"""

# input for first question
choose = raw_input(">> ")

# here if it is supposed to be a file input
if (choose == "1"):
	print "Very well! Please choose a file to be loaded."
	
	file_check = 0
	while (file_check != 1):
		filename = raw_input(">> ")
		try:
			txt = open(filename)
			print "Here is your file %r:" % filename
			text = txt.read()
			text = text.decode('utf-8')
			txt.close()
			print "Name: ", txt.name
			print "Closed: ", txt.closed
			print "Opening mode: ", txt.mode
			print "Softspace: ", txt.softspace
			file_check = 1
		except:
			print "This file doesnot seem to exist. Or maybe you had a typo in your path. Doesn't matter - please try again."

# this will be executed if user wants to paste a text into the console
elif (choose == "2"):
	text = raw_input("Past your text here and press enter >> ")

# this is for stupid people who do not read the instructions
else:
	print "You do not seem read instructions, sorry - I cannot handle your input."

if (text):
	liste = sentences.chunk_sentences(text)
	print "Output of Sentence Chunk: "
	print liste
	print "   "
	tokens = tokens.tokens(liste)
	print "Output of Tokeniser: "
	print tokens
	print "   "
	annotated_text = annotation.Annotator(tokens)
	print "Output of Annotator: "
	print annotated_text
	print "   "
	lemmatised_text = lemma.lemmatizer(annotated_text)
	print "Output of Lemmatizer: "
	print lemmatised_text
else:
	print "There is no text, I am very sorry about this!"






