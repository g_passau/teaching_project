import nltk
import annotation as an
import pdb

def Annotator(list):
    Outputlist = []
    for sentence in list:
        Outputlist.append(nltk.pos_tag(sentence))
    return Outputlist

if __name__=='__main__':
    InputAnno = []
    InputAnno.append([u'This', u'is', u'an', u'example', u'sentence'])
    InputAnno.append([u'All', u'the', u'ducks', u'are', u'swimming', u'in', u'the', u'water'])
    InputAnno.append([u'And', u'now', u'for', u'something', u'completely', u'different'])
    print Annotator(InputAnno)
