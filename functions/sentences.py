import nltk.data
def chunk_sentences(text):
	# select the right tokenizer
	tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
	# do the job and return
	#print tokenizer.tokenize(text)
	return tokenizer.tokenize(text)
	