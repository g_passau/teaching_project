import nltk

Input = [[(u'This', 'DT'), (u'is', 'VBZ'), (u'an', 'DT'), (u'example', 'NN'), (u'sentence', 'NN')],
 [(u'All', 'PDT'), (u'the', 'DT'), (u'ducks', 'NNS'), (u'are', 'VBP'), (u'swimming', 'VBG'), (u'in', 'IN'), (u'the', 'DT'), (u'water', 'NN')],
 [(u'And', 'CC'), (u'now', 'RB'), (u'for', 'IN'), (u'something', 'NN'), (u'completely', 'RB'), (u'different', 'JJ')]]

def lemmatizer(list):
    Output = []
    LMMTZR = nltk.stem.WordNetLemmatizer()
    for sentence in Input:
        words = [i[0] for i in sentence]
        Lemmalist = []
        for i in words:
            lemma = LMMTZR.lemmatize(i).lower()
            Lemmalist.append(lemma)
        Output.append(Lemmalist)
    return Output

