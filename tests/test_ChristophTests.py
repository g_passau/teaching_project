import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('./functions')

import nltk
import annotation as an
import lemma as le
import pdb

def test_AnnoTest():
    InputAnno = []
    InputAnno.append([u'This', u'is', u'an', u'example', u'sentence'])
    InputAnno.append([u'All', u'the', u'ducks', u'are', u'swimming', u'in', u'the', u'water'])
    InputAnno.append([u'And', u'now', u'for', u'something', u'completely', u'different'])
    Expectedoutput = [[(u'This', 'DT'), (u'is', 'VBZ'), (u'an', 'DT'), (u'example', 'NN'), (u'sentence', 'NN')],
 [(u'All', 'PDT'), (u'the', 'DT'), (u'ducks', 'NNS'), (u'are', 'VBP'), (u'swimming', 'VBG'), (u'in', 'IN'), (u'the', 'DT'), (u'water', 'NN')],
 [(u'And', 'CC'), (u'now', 'RB'), (u'for', 'IN'), (u'something', 'NN'), (u'completely', 'RB'), (u'different', 'JJ')]]
    FunctionOutput = an.Annotator(InputAnno)
    assert Expectedoutput == FunctionOutput



def test_LemmaTest():
    InputLemma = [[(u'This', 'DT'), (u'is', 'VBZ'), (u'an', 'DT'), (u'example', 'NN'), (u'sentence', 'NN')],
 [(u'All', 'PDT'), (u'the', 'DT'), (u'ducks', 'NNS'), (u'are', 'VBP'), (u'swimming', 'VBG'), (u'in', 'IN'), (u'the', 'DT'), (u'water', 'NN')],
 [(u'And', 'CC'), (u'now', 'RB'), (u'for', 'IN'), (u'something', 'NN'), (u'completely', 'RB'), (u'different', 'JJ')]]
    ExpectedLemmaoutput = [[u'this', u'is', u'an', u'example', u'sentence'], [u'all', u'the', u'duck', u'are', u'swimming', u'in', u'the', u'water'], [u'and', u'now', u'for', u'something', u'completely', u'different']]
    FunctionLemmaOutput = le.lemmatizer(InputLemma)
    assert ExpectedLemmaoutput == FunctionLemmaOutput

if __name__=='__main__':
    test_AnnoTest()

