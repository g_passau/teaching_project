#!/usr/bin/env python
# -*- coding: utf-8 -*-
# import ...

import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('../functions')

import functions.sentences as a
import functions.tokens as t

def test_chunk_sentences():
    input = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et malesuada felis, non accumsan sem. Aenean gravida velit eget orci pretium malesuada. Integer lacus dolor, dictum eget ex nec, convallis pharetra nisl."""
    expected_result = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit." ,"Aenean et malesuada felis, non accumsan sem.", "Aenean gravida velit eget orci pretium malesuada.", "Integer lacus dolor, dictum eget ex nec, convallis pharetra nisl."]

    results = []
    results = a.chunk_sentences(input)

    assert results == expected_result

def test_tokens():
    input = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit." ,"Aenean et malesuada felis, non accumsan sem.", "Aenean gravida velit eget orci pretium malesuada.", "Integer lacus dolor, dictum eget ex nec, convallis pharetra nisl."]
    expected_result = [['Lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipiscing', 'elit'], ['Aenean', 'et', 'malesuada', 'felis', 'non', 'accumsan', 'sem'], ['Aenean', 'gravida', 'velit', 'eget', 'orci', 'pretium', 'malesuada'], ['Integer', 'lacus', 'dolor', 'dictum', 'eget', 'ex', 'nec', 'convallis', 'pharetra', 'nisl']]

    results = []
    results = t.tokens(input)

    assert results == expected_result